FROM nvidia/cuda:9.0-base-centos7 as build

RUN yum install -y \
        gcc \
        g++ \
        wget \
        cuda-cudart-dev-9-0 \
        cuda-misc-headers-9-0 \
        cuda-nvml-dev-9-0 && \
    rm -rf /var/cache/yum/*

ENV GOLANG_VERSION 1.9.2
RUN wget -nv -O - https://storage.googleapis.com/golang/go${GOLANG_VERSION}.linux-amd64.tar.gz \
    | tar -C /usr/local -xz
ENV GOPATH /go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH

ENV CGO_CFLAGS "-I /usr/local/cuda-9.0/include"
ENV CGO_LDFLAGS "-L /usr/local/cuda-9.0/lib64"
ENV PATH=$PATH:/usr/local/nvidia/bin:/usr/local/cuda/bin

WORKDIR /go/src/nvidia-device-plugin
COPY . .

RUN go install -ldflags="-s -w" -v nvidia-device-plugin


FROM fedora:27 as upx

COPY --from=build /go/bin/nvidia-device-plugin /usr/bin/nvidia-device-plugin

RUN dnf install -y upx

RUN upx --brute /usr/bin/nvidia-device-plugin


FROM scratch

COPY --from=build ["/usr/lib64/libdl.so.2", "/usr/lib64/libpthread.so.0", "/usr/lib64/libc.so.6", "/usr/lib64/"]
COPY --from=build ["/lib64/ld-linux-x86-64.so.2", "/lib64/"]
COPY --from=upx /usr/bin/nvidia-device-plugin /usr/bin/nvidia-device-plugin

ENV NVIDIA_VISIBLE_DEVICES=all
ENV NVIDIA_DRIVER_CAPABILITIES=utility

CMD ["nvidia-device-plugin"]
